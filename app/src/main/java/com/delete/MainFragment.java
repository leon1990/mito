package com.delete;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Window;

import com.library.pagerindicator.TabPageIndicator;
import com.mimi.mito.FactoryFragment;
import com.mimi.mito.R;

public class MainFragment extends FragmentActivity {

    // girl |Cougar | lady | Beauty | selfie | emotion
    private static final String[] CONTENT = new String[]{"Emotion", "Essay", "model", "Girl", "Lady", "Cougar"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setTheme(R.style.Theme_PageIndicatorDefaults);
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gallery);
        FragmentPagerAdapter adapter = new BeautyPicAdapter(getSupportFragmentManager());
        ViewPager pager = findViewById(R.id.pager);
        pager.setAdapter(adapter);
        TabPageIndicator indicator = findViewById(R.id.indicator);
        indicator.setViewPager(pager);
    }

    class BeautyPicAdapter extends FragmentPagerAdapter {
        public BeautyPicAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return FactoryFragment.createFragment(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length];
        }

        @Override
        public int getCount() {
            return CONTENT.length;
        }
    }


}
