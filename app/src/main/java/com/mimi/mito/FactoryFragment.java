package com.mimi.mito;

import android.util.SparseArray;

import com.mimi.mito.fragment.pager.ModelFragment;
import com.mimi.mito.fragment.pager.CougarFragment;
import com.mimi.mito.fragment.pager.GirlFragment;
import com.mimi.mito.fragment.pager.LadyFragment;

/**
 * Created by Administrator on 2017/9/17.
 */

public class FactoryFragment {

    private static SparseArray<BaseFragment> mFragments = new SparseArray<BaseFragment>();

    public static BaseFragment createFragment(int position) {
        BaseFragment fragment = mFragments.get(position);
        if (fragment == null) {
            switch (position) {
                case 0:
                    fragment = new ModelFragment();
                    break;
                case 1:
                    fragment = new GirlFragment();
                    break;
                case 2:
                    fragment = new LadyFragment();
                    break;
                case 3:
                    fragment = new CougarFragment();
                    break;
                default:
                    break;
            }
            if (fragment != null) {
                mFragments.put(position, fragment);
            }
        }

        return fragment;
    }

}
