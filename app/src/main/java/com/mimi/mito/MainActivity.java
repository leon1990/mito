package com.mimi.mito;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.RadioGroup;

import com.mimi.mito.fragment.EssayFragment;
import com.mimi.mito.fragment.GalleryFragment;
import com.mimi.mito.fragment.HomeFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<BaseFragment> mFragment;
    public FrameLayout frameLayout;
    private RadioGroup radioGroup;
    private FragmentTransaction fragmentTransaction;
    private int position;
    private BaseFragment mConten;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        this.frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frameLayout, new HomeFragment()).commit();
        mFragment = new ArrayList<>();
        mFragment.add(new HomeFragment());
        mFragment.add(new EssayFragment());
        mFragment.add(new GalleryFragment());
        setRadioGroupListener();
    }

    private void setRadioGroupListener() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_home:
                        position = 0;
                        break;
                    case R.id.rb_shopping:
                        position = 1;
                        break;
                    case R.id.rb_my:
                        position = 2;
                        break;
                    default:
                        position = 0;
                        break;
                }
                BaseFragment toFragment = getFragment();
                replaceFragment(mConten, toFragment);
            }
        });
    }

    private void replaceFragment(BaseFragment from, BaseFragment to) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (from != to) {
            mConten = to;
            if (!to.isAdded()) {
                if (from != null) {
                    ft.hide(from);
                }
                ft.add(R.id.frameLayout, to);
                ft.show(to).commit();
            } else {
                if (from != null) {
                    ft.hide(from);
                }
                ft.show(to).commit();
            }
        }
    }

    public BaseFragment getFragment() {
        return mFragment.get(position);
    }
}
