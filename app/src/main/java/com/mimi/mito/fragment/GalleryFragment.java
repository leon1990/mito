package com.mimi.mito.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.library.pagerindicator.TabPageIndicator;
import com.mimi.mito.BaseFragment;
import com.mimi.mito.FactoryFragment;
import com.mimi.mito.R;


public class GalleryFragment extends BaseFragment {

    // girl |Cougar | lady | Beauty | selfie | emotion
    private static final String[] CONTENT = new String[]{"model", "Girl", "Lady", "Cougar"};

    public GalleryFragment() {
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.activity_gallery, null, false);

        FragmentPagerAdapter adapter = new BeautyPicAdapter(getActivity().getSupportFragmentManager());
        ViewPager pager = layout.findViewById(R.id.pager);
        pager.setAdapter(adapter);
        TabPageIndicator indicator = layout.findViewById(R.id.indicator);
        indicator.setViewPager(pager);
        return layout;
    }

    class BeautyPicAdapter extends FragmentPagerAdapter {
        public BeautyPicAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return FactoryFragment.createFragment(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length];
        }

        @Override
        public int getCount() {
            return CONTENT.length;
        }
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {

    }
}
