package com.mimi.mito.fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimi.mito.R;

/**
 * Created by Administrator on 2017/9/17.
 */

public class BeautyAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;

    public BeautyAdapter(Context mContext) {
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);

    }

    @Override
    public int getCount() {
        return 23;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_beauty, null);
            viewHolder.beauty_pic = (ImageView) convertView.findViewById(R.id.beauty_item_pic);
            viewHolder.beauty_heart = (ImageView) convertView.findViewById(R.id.beauty_item_heart);
            viewHolder.total_click = (TextView) convertView.findViewById(R.id.beauty_item_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.beauty_pic.setBackgroundResource(R.mipmap.beauty_item_pic);
        viewHolder.total_click.setText("456789");
        return convertView;
    }

    class ViewHolder {
        ImageView beauty_pic;
        ImageView beauty_heart;
        TextView total_click;
    }

}
