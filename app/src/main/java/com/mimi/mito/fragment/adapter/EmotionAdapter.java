package com.mimi.mito.fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimi.mito.R;

/**
 * Created by Administrator on 2017/9/17.
 */

public class EmotionAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;

    public EmotionAdapter(Context mContext) {
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return 23;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_emotion, null);
            viewHolder.emotion_pic = (ImageView) convertView.findViewById(R.id.emotion_item_pic);
            viewHolder.title_text = (TextView) convertView.findViewById(R.id.emotion_item_text);
            viewHolder.tv_no_id = (TextView) convertView.findViewById(R.id.tv_no_id);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.emotion_pic.setBackgroundResource(R.mipmap.emotion_item_pic);
        viewHolder.title_text.setText("...坏习惯蝴蝶效应....原谅家暴男人就是助纣为虐...");
        viewHolder.tv_no_id.setText("" + position);
        return convertView;
    }

    class ViewHolder {
        ImageView emotion_pic;
        TextView title_text;
        TextView tv_no_id;
    }
}
