package com.mimi.mito.fragment.pager;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.mimi.mito.BaseFragment;
import com.mimi.mito.R;
import com.mimi.mito.activity.BeautyAPhotoBrowse;
import com.mimi.mito.fragment.adapter.BeautyAdapter;

public final class ModelFragment extends BaseFragment {

    private static final String KEY_CONTENT = "TestFragment:Content";

    public static ModelFragment newInstance(String content) {
        ModelFragment fragment = new ModelFragment();

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 20; i++) {
            builder.append(content).append(" ");
        }
        builder.deleteCharAt(builder.length() - 1);
        fragment.mContent = builder.toString();

        return fragment;
    }

    private String mContent = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_model, container, false);


        GridView gridView = (GridView) layout.findViewById(R.id.gv_beauty_id);
        gridView.setAdapter(new BeautyAdapter(getActivity()));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), BeautyAPhotoBrowse.class);
                getActivity().startActivity(intent);
            }
        });
        return layout;
    }


    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }
}
